import React from 'react';
import './style.css';

const Contact = () => {
  return (
    <div id="contact" className="row contact-bar">
      <div>
        <div className="contents" >
          <div>
            <div className="title-size">Ready to join?</div>
          </div>
          <div className="text-center" >
            <div>
              <div className="title-size">Get the latest updates</div>
            </div>
            <div>
              <div className="subscribe-size">Subscribe for updates on new talents</div>
            </div>
          </div>
        </div>
        <div className="contents" >
          <div className="btn-join-center" >
            <button className="btn-custom btn-join">Join as a talent</button>
          </div>
          <div className="form-group" >
            <div>
              <input
                type="email"
                id="email"
                className="form-control"
                placeholder="Email"
                required="required"
              />
            </div>
            <button className="btn-go" />
          </div>
        </div>
        
      </div>
      <div class="social-media-block">
        <ul class="social-media-icons">
          <li className="social-padding">
            <a href="https://www.instagram.com/stardmofficial/"
              className="icon-instagram"
              title="Instagram"
              target="_blank"
              rel="noopener noreferrer">
              <span>instagram</span>
            </a>
          </li>
          <li className="social-padding">
            <a href="https://www.twitter.com/stardmofficial/"
              className="icon-twitter"
              title="Twitter"
              target="_blank"
              rel="noopener noreferrer">
              <span>twitter</span>
            </a>
          </li>
          <li className="social-padding">
            <a href="https://www.facebook.com/Stardm-107648671168873"
              className="icon-facebook"
              title="Facebook"
              target="_blank"
              rel="noopener noreferrer">
              <span>facebook</span>
            </a>
          </li>
        </ul>
      </div>
      <div class="copy-right">
        @2020 STARDM
      </div>
    </div>
  )
}

export default Contact;