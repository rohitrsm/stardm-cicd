import React from "react";
import './style.css';
import logstar from "../../assets/img/logo-star.png";
import {HOME_PAGE} from "../../config";

const Navigation = () => {
  const handleLogoClick = () => {
    window.open(HOME_PAGE, "_blank");
  }

  return (
    <div id="navigation" className="row navbar">
      <div className="logo-star" onClick={handleLogoClick}>
        <img
          src={logstar}
          className="logo-star"
          alt="Project Title"
        />
      </div>
      <div class="social-media-block">
        <ul class="social-media-icons">
          <li>
            <a href="https://www.instagram.com/stardmofficial/"
              className="icon-instagram"
              title="Instagram"
              target="_blank"
              rel="noopener noreferrer">
              <span>instagram</span>
            </a>
          </li>
          <li>
            <a href="https://www.twitter.com/stardmofficial/"
              className="icon-twitter"
              title="Twitter"
              target="_blank"
              rel="noopener noreferrer">
              <span>twitter</span>
            </a>
          </li>
          <li>
            <a href="https://www.facebook.com/Stardm-107648671168873"
              className="icon-facebook"
              title="Facebook"
              target="_blank"
              rel="noopener noreferrer">
              <span>facebook</span>
            </a>
          </li>
          <li>
            <a href={HOME_PAGE}
              className="icon-menu"
              title="menu"
              target="_blank"
              rel="noopener noreferrer">
              <span>menu</span>
            </a>
          </li>
        </ul>
      </div>
    </div>

  );
}

export default Navigation;
