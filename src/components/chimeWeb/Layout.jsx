import React from 'react';
import Navigation from './Navigation';
import Contact from './Contact';

const Layout = (props) => {
  const { children } = props;
  return (
    <div>
      <Navigation />
      {children}
      <Contact />
    </div>
  )
}

export default Layout;