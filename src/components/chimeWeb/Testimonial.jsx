import React, { useState, useEffect } from 'react';
import './style.css';

const Testimonial = () => {

  useEffect(() => {
    // Update the document title using the browser API    
    });

  return (
    <div id="testimonial" >
      <div className="section-group">
          <h3 className="section-header">{localStorage.getItem('streamingTag')}</h3>
          <h1>{localStorage.getItem('streamingTitle')}</h1>
          <h3>{localStorage.getItem('streamingTime')}</h3>
          <div className="left-text" dangerouslySetInnerHTML={{ __html: localStorage.getItem('streamingDescription') }} ></div>
      </div>
    </div>
  );
}

export default Testimonial;