import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

class End extends Component {

  constructor() {
    super();
    this.baseHref = localStorage.getItem("starusername");
  }

  render() {
    return (
      <div className="welcome form-grid">
        <div className="welcome__intro">
          <div className="intro__inner formatted-text">
            <h1>STARDM TEST</h1>
            <h3>Create or join rooms, and watch main streams with anyone.</h3>
          </div>
        </div>

        <div className="welcome__content pd-4">
          <div className="content__inner formatted-text">
            <h2 className="mg-b-2">Room closed</h2>
            <p>The host has ended the meeting and closed the room.</p>
            <a href={`/`} className="mg-t-3 btn btn--primary">Go to room again</a>
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(End);
