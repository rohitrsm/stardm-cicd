import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as config from '../../config';

import {
  MeetingSessionStatusCode
} from 'amazon-chime-sdk-js';

// Components
import VideoPlayer from '../videoPlayer/VideoPlayer';
import ChatChime from '../chat/ChatChime';
// import Controls from './Controls';
import Settings from './Settings';
import LocalVideo from './LocalVideo';
import RemoteVideoGroup from './RemoteVideoGroup';
import Error from './Error';
import Layout from './Layout';
import Testimonial from './Testimonial';
//redux
import { connect } from 'react-redux';
import { standardRoom, vipRoom, creatRoom } from '../Action';
import CircularProgress from '@material-ui/core/CircularProgress';
//change event name
import { Helmet } from 'react-helmet'
// Styles
import './ChimeWeb.css';
import './style.css';

class Meeting extends Component {

  
  constructor() {
    super();
    this.disconnectMasterRoom = this.disconnectMasterRoom.bind(this);

    this.state = {
      meetingStatus: null, // Loading, Success or Failed
      showSettings: false,
      showError: false,
      errorMsg: '',
      activeChatRoomIndex: JSON.parse(sessionStorage.getItem('activeChatRoomIndex')),
      isGlobalCreatedRoom: JSON.parse(sessionStorage.getItem('isGlobalCreatedRoom')),
      loadingStatus: false,
      joinInfo: null,
      role: '',
      username: '',
      title: '',
      ssName: '',
    }

    this.baseHref = localStorage.getItem("starusername");
    this.eventName = localStorage.getItem('eventName');
    this.audioElementRef = React.createRef();
    this.myVideoElement = React.createRef();    
    this.playbackURL = localStorage.getItem('playbackURL');
    this.username = localStorage.getItem("username");    
  }

  componentDidMount() {
    if (this.state.isGlobalCreatedRoom) {
      this.start();
    }    
  }

  componentDidUpdate(prevProps) {
    if (this.props.tabGlobalIndex !== 0 && this.props.tabGlobalIndex != prevProps.tabGlobalIndex) {
      const activeChatRoomIndex = JSON.parse(sessionStorage.getItem('activeChatRoomIndex'));
      const isGlobalCreatedRoom = JSON.parse(sessionStorage.getItem('isGlobalCreatedRoom'));
      this.setState({
        isGlobalCreatedRoom,
        activeChatRoomIndex,
      })
      const room = localStorage.getItem("title");
      const ssName = `chime[${room}]`;
      const ssData = JSON.parse(sessionStorage.getItem(ssName));
      if (ssData && ssData.joinInfo)
        this.setState({
          joinInfo: ssData.joinInfo
        })
    }
  }

  start = async () => {
    try {
      const activeChatRoomIndex = JSON.parse(sessionStorage.getItem("activeChatRoomIndex"));
      this.setState({ loadingStatus: true });
      const qs = new URLSearchParams(this.props.location.search);
      // const room = qs.get('room');
      const room = localStorage.getItem("title");
      const ssName = `chime[${room}]`;
      if (!room || !sessionStorage.getItem(ssName)) {
        this.props.history.push(`/`);
      }
      const ssData = await JSON.parse(sessionStorage.getItem(ssName));

      if (config.DEBUG) console.log(ssData);

      const username = ssData.username;
      const title = ssData.title;
      const role = ssData.role;
      let joinInfo = null;
      const subjectName = ssData.subjectName;
      const subjectDescription = ssData.subjectDescription;
      const avatarURL = ssData.avatarURL;
      const isConference = ssData.isConference;

      if (!ssData.joinInfo) {
        let user_name = username.replaceAll(" ", "-");
        try {
          joinInfo = await this.props.chime.createRoom(role, user_name, title, ssData.playbackURL, subjectName, subjectDescription, avatarURL, isConference);
          const data = {
            ...ssData,
            joinInfo: joinInfo
          }
          sessionStorage.setItem(ssName, JSON.stringify(data));
          this.playbackURL = joinInfo.PlaybackURL;
        } catch (error) {
          sessionStorage.setItem("activeChatRoomIndex", -1);
        }
      } else {
        // Browser refresh
        joinInfo = ssData.joinInfo;
        this.playbackURL = ssData.joinInfo.PlaybackURL;
        if (isConference)
          await this.props.chime.reInitializeMeetingSession(joinInfo, username)
      }

      if (isConference) {
        await this.props.chime.audioVideo.addObserver({
          audioVideoDidStop: async (sessionStatus) => {
            if (sessionStatus.statusCode() === MeetingSessionStatusCode.AudioCallEnded) {
              const whereTo = `/${role === 'host' ? '' : 'end'}`;
              this.props.chime.leaveRoom(role === 'host');
              this.props.history.push(whereTo);
            }
          }
        });
      }
      await new Promise(resolve => setTimeout(resolve, 1500));
      this.setState({ meetingStatus: 'Success', username, title, role, joinInfo, ssName, activeChatRoomIndex });
      await new Promise(resolve => setTimeout(resolve, 50));
      if (isConference) {
        await this.props.chime.joinRoom(this.audioElementRef.current);
      }
      this.setState({ loadingStatus: false });
    } catch (error) {
      // eslint-disable-next-line
      console.error(error);
      this.setState({ meetingStatus: 'Failed', loadingStatus: false });
    }
  };

  handleTabChange = () => {
    const room = localStorage.getItem("title");
    const ssName = `chime[${room}]`;
    const ssData = JSON.parse(sessionStorage.getItem(ssName));
    if (ssData && ssData.joinInfo)
      this.setState({
        joinInfo: ssData.joinInfo
      })
  }

  handleExitRoom = () => {
    this.setState({
      activeChatRoomIndex: -1,
      meetingStatus: null,
    });  
  }
  
  disconnectMasterRoom = () => {
    this.props.standardRoom();
    sessionStorage.clear();
    sessionStorage.setItem("activeChatRoomIndex", -1);  
    this.setState({
      activeChatRoomIndex: -1,
      meetingStatus: null,
    });
  }

  endMasterChatRoom = () => {
   this.setState({
     isGlobalCreatedRoom: false,
     meetingStatus: null,
   });
 }  /*
   * Settings
   */

  openSettings = () => {
    this.setState({ showSettings: true });
  }

  closeSettings = () => {
    this.setState({ showSettings: false });
  }

  handleClick = (e) => {
    const { showSettings } = this.state;
    if (showSettings) {
      let node = e.target;
      let isModal = false;
      while (node) {
        if (node && node.classList && node.classList.contains('modal__el')) {
          isModal = true;
          break;
        }
        node = node.parentNode;
      }
      if (!isModal) {
        this.setState({ showSettings: false });
      }
    }
  }

  saveSettings = (playbackURL, currentAudioInputDevice, currentAudioOutputDevice, currentVideoInputDevice) => {
    this.setState({
      showSettings: false,
      currentAudioInputDevice,
      currentAudioOutputDevice,
      currentVideoInputDevice
    });
  }

  setMetadataId = (metadataId) => {
    this.setState({ metadataId });
  }

  setErrorMsg = errorMsg => {
    this.setState({ errorMsg, showError: true });
  }

  closeError = () => {
    this.setState({ showError: false });
  }

  layout = () => {
    // if (this.state.meetingStatus !== 'Success') {
    //   this.props.history.push(`/${this.baseHref}/`);
    //   return;
    // }
    const { activeChatRoomIndex, joinInfo, username, title, ssName, role, meetingStatus } = this.state;
    const isGlobalCreatedRoom = JSON.parse(sessionStorage.getItem("isGlobalCreatedRoom"));
    return (
      <Layout>
        <div className="app-grid" onClick={this.handleClick}>
          <div className="main-stage">
            <VideoPlayer
              setMetadataId={this.setMetadataId}
              videoStream={this.playbackURL}
            />
            {this.props.tabGlobalIndex === 1 && activeChatRoomIndex >= 0 && meetingStatus === 'Success' && joinInfo && joinInfo.isConference &&
              <div className="cams pos-relative">
                <LocalVideo
                  chime={this.props.chime}
                  joinInfo={joinInfo}
                />
                <RemoteVideoGroup
                  chime={this.props.chime}
                  joinInfo={joinInfo}
                  disconnectMasterRoom = {() => this.disconnectMasterRoom()}
                />
              </div>
            }
            {this.props.tabGlobalIndex === 2 && isGlobalCreatedRoom && meetingStatus === 'Success' && joinInfo && joinInfo.isConference &&
              <div className="cams pos-relative">
                <LocalVideo
                  chime={this.props.chime}
                  joinInfo={joinInfo}
                />
                <RemoteVideoGroup
                  chime={this.props.chime}
                  joinInfo={joinInfo}
                />
              </div>
            }
            <Testimonial />
          </div>
            <ChatChime              
              chime={this.props.chime}
              title={title}
              username={username}
              joinInfo={joinInfo}
              baseHref={this.baseHref}
              ssName={ssName}
              role={role}
              history={this.props.history}
              myVideoElement={this.myVideoElement}
              openSettings={this.openSettings}
              start={() => this.start()}
              handleTabChange = {() => this.handleTabChange()}
              handleExitRoom = {() => this.handleExitRoom()}
              endMasterChatRoom = {() => this.endMasterChatRoom()}
              meetingStatus = {this.state.meetingStatus}
              activeChatRoomIndex = {this.state.activeChatRoomIndex}
              isConference = {joinInfo ? joinInfo.isConference : false}
            />
          {this.state.showSettings && (
            <Settings
              chime={this.props.chime}
              joinInfo={joinInfo}
              saveSettings={this.saveSettings}
            />
          )}
        </div>
      </Layout>
    )
  }

  render() {
    return (
      
      <React.Fragment>
         <Helmet>
          <title>{ this.eventName }</title>
        </Helmet>
        <audio ref={this.audioElementRef} style={{ display: 'none' }} />
        {
          this.layout()
        }
        {this.state.showError && (
          <Error
            closeError={this.closeError}
            errorMsg={this.state.errorMsg}
          />
        )}
        { this.state.loadingStatus
          ?
          <div style={{ display: 'flex', alignItems: 'center', width: '100%', height: '100%', justifyContent: 'center', position: 'absolute', top: '0' }}>
            <CircularProgress />
          </div>
          :
          ''
        }
      </React.Fragment>
    )
  }
}

Meeting.propTypes = {
  chime: PropTypes.object
};

function mapState(state) {
  return {
    tabGlobalIndex: state.tabGlobalIndex
  };
}

const mapDispatch = {
  standardRoom, vipRoom, creatRoom
};

export default withRouter(connect(mapState, mapDispatch)(Meeting));
