import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import * as config from '../../config';
import Error from './Error';
import decrypt from '../../utils/decrypt'
import Cookies from 'js-cookie';
import queryString from 'query-string';

class Login extends Component {

  state = {    
    isAdmin: false,
    username: '',
    starusername: '',
    avatar: 'https://brainshares.s3-us-west-2.amazonaws.com/1599929985_126726_avatar1.jpg',
    playbackURL: config.DEFAULT_VIDEO_STREAM,
    level: 0,
    ivsId: 'room_test',
    eventName: 'STARDM',
    errorMsg: '',
    showError: false,
    streamingTag: '#LoremLorem #Lorem',
    streamingTitle: 'Lorem ipsum dolor sit amet',
    streamingTime: 'March 6th, 2021 8:20pm ET',
    streamingDescription:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at.',
  }

  constructor(props) {
    super (props);
    this.inputRef = React.createRef();
    sessionStorage.clear();
    localStorage.clear();

    let username = Cookies.get('username') ? decrypt(Cookies.get('username')) : "";
    // return ;
    let membership_status = Cookies.get('membership_status') ? decrypt(Cookies.get('membership_status')) : "";
    let ticket_type = Cookies.get('ticket_type') ? decrypt(Cookies.get('ticket_type')) : "";
    let event_id = Cookies.get('event_id') ? decrypt(Cookies.get('event_id')) : "";
    let event_title = Cookies.get('event_title') ? decrypt(Cookies.get('event_title')) : "";
    let event_description = Cookies.get('event_description') ? decrypt(Cookies.get('event_description')) : "";
    let event_datetime = Cookies.get('event_datetime') ? decrypt(Cookies.get('event_datetime')) : "";
    let has_ticket = Cookies.get('has_ticket') ? decrypt(Cookies.get('has_ticket')) : "";
    let stream_url = Cookies.get('stream_url') ? decrypt(Cookies.get('stream_url')) : "";
    let level;
    let room_id = this.props.location.search ? queryString.parse(this.props.location.search)["room-id"] : null;

    if (ticket_type == 4) {
      return ;
    }

    if (!room_id) {
      window.location.replace(config.HOME_PAGE);
      return ;
    }


    if (!has_ticket) {
      window.location.replace(config.HOME_PAGE);
      return ;
    }

    if (ticket_type >= 2) {
      level = 2;
    }
    else if (ticket_type == 1) {
      level = 1;
    }
    else if (ticket_type == 0) {
      level = 0;
    }
    else if (username === "" || stream_url === "" || event_title === "" || event_description === "" || event_datetime === ""){
      window.location.replace(config.HOME_PAGE + "/redirect/?redirect=" + window.location.origin + "&room-id=" + room_id);
      return ;
    }

    localStorage.setItem("username", username);
    localStorage.setItem("avatar", "");
    localStorage.setItem("level", level);
    localStorage.setItem("playbackURL", stream_url);
    const temp = stream_url.split('.');
    const room_name = temp[temp.length-2];
    localStorage.setItem("starusername", room_name);
    localStorage.setItem("ivs_id", room_name);
    localStorage.setItem("eventName", event_id);
    //streaming description
    // localStorage.setItem('streamingTag', streamingTag);
    localStorage.setItem('streamingTitle', event_title);
    localStorage.setItem('streamingTime', event_datetime);
    localStorage.setItem('streamingDescription', event_description);
    props.history.push(`/${room_name}/`);
  }

  componentDidMount() {
    this.setState({
      isAdmin: true
    })
  }

  handleNameChange = e => {
    this.setState({ username: e.target.value })
  }

  handleStarNameChange = e => {
    this.setState({ starusername: e.target.value })
  }

  handleLevelChange = e => {
    this.setState({ level: e.target.value })
  }

  handlePlaybackURLChange = e => {
    this.setState({ playbackURL: e.target.value })
  }

  handleIvsIdChange = e => {
    this.setState({ ivsId: e.target.value })
  }

  handleEventNameChange = e => {
    this.setState({ eventName: e.target.value })
  }

  handleAvatarChange = e => {
    this.setState({ avatar: e.target.value })
  }

  handleStreamingTagChange = e => {
    this.setState({ streamingTag: e.target.value })
  }

  handleStreamingTitleChange = e => {
    this.setState({ streamingTitle: e.target.value })
  }

  handleStreamingTimeChange = e => {
    this.setState({ streamingTime: e.target.value })
  }

  handleStreamingDescriptionChange = e => {
    this.setState({ streamingDescription: e.target.value })
  }

  handleLoginRoom = (e) => {
    e.preventDefault();
    e.stopPropagation();
    this.loginRoom();
  }

  setErrorMsg = errorMsg => {
    this.setState({ errorMsg, showError: true });
  }

  closeError = () => {
    this.setState({ showError: false });
  }

  async loginRoom() {
    //Move home.js
    const { level, avatar, ivsId, username, playbackURL, eventName, streamingTag, streamingTitle, streamingTime, streamingDescription } = this.state;
    let { starusername } = this.state;
    starusername = starusername.replaceAll(/\s/g, '');
    localStorage.clear();
    sessionStorage.clear(); 
    localStorage.setItem("username", username);
    localStorage.setItem("avatar", avatar);
    localStorage.setItem("level", level);
    localStorage.setItem("playbackURL", playbackURL);
    const temp = playbackURL.split('.');
    const room_name = temp[temp.length-2];
    localStorage.setItem("starusername", room_name);
    localStorage.setItem("ivs_id", room_name);
    localStorage.setItem("eventName", eventName);
    //streaming description
    localStorage.setItem('streamingTag', streamingTag);
    localStorage.setItem('streamingTitle', streamingTitle);
    localStorage.setItem('streamingTime', streamingTime);
    localStorage.setItem('streamingDescription', streamingDescription);
    this.props.history.push(`/${room_name}/`);
  }

  render() {
    const { username, playbackURL, avatar, ivsId, level, starusername, eventName, streamingTag, streamingTitle, streamingTime, streamingDescription } = this.state;
    const createRoomDisabled = !username ;
    return (
      this.state.isAdmin && <React.Fragment>
        <div className="welcome form-grid">

          <div className="welcome__intro">
            <div className="intro__inner formatted-text">
              <h1>STARDM TEST</h1>
              <h3>Create or join rooms, and watch main streams with anyone.</h3>
            </div>
          </div>

          <div className="welcome__content pd-4">
            <div className="content__inner">
              <h2 className="mg-b-2">This information will be received from cookie</h2>
              <form action="">
                <fieldset className="mg-b-2">
                  <input type="text" placeholder="user name" value={username} ref={this.inputRef} onChange={this.handleNameChange} />
                  {/* <input type="text" placeholder="star user name" value={starusername} onChange={this.handleStarNameChange} /> */}
                  {/* <input type="text" placeholder="Avatar URL" value={avatar} onChange={this.handleAvatarChange} /> */}
                  <select id="framework" onChange={this.handleAvatarChange}>
                    <option value="https://brainshares.s3-us-west-2.amazonaws.com/1599929985_126726_avatar1.jpg" >Avatar 1</option>
                    <option value="https://brainshares.s3-us-west-2.amazonaws.com/1599929274_432292_avatar2.jpg" >Avatar 2</option>
                    <option value="https://brainshares.s3-us-west-2.amazonaws.com/1599930148_769708_avatar3.jpg" >Avatar 3</option>
                    <option value="https://brainshares.s3-us-west-2.amazonaws.com/1599930106_935123_avatar4.jpg" >Avatar 4</option>
                    <option value="https://brainshares.s3-us-west-2.amazonaws.com/1599982759_119196_1.jpg" >Avatar 5</option>
                    <option value="https://brainshares.s3-us-west-2.amazonaws.com/1599956910_745434_3.jpg" >Avatar 6</option>
                  </select>
                  <input type="text" placeholder="Event Name" value={eventName} onChange={this.handleEventNameChange} />
                  {/* <input type="text" placeholder="IVS ID" value={ivsId} onChange={this.handleIvsIdChange} /> */}
                  <input type="text" placeholder="Level" value={level} onChange={this.handleLevelChange} />
                  <input type="text" placeholder="Playback URL" value={playbackURL} onChange={this.handlePlaybackURLChange} />
                  <input type="text" placeholder="Streaming Tag" value={streamingTag} onChange={this.handleStreamingTagChange} />                 
                  <input type="text" placeholder="Streaming Title" value={streamingTitle} onChange={this.handleStreamingTitleChange} />
                  <input type="text" placeholder="Streaming Time" value={streamingTime} onChange={this.handleStreamingTimeChange} />
                  {/* <input type="text" placeholder="Streaming Description" row="5" value={streamingDescription} onChange={this.handleStreamingDescriptionChange} /> */}
                  <textarea rows="5" placeholder="Streaming Description" row="3" value={streamingDescription} onChange={this.handleStreamingDescriptionChange}/>
                  <button className="mg-t-2 btn btn--primary" disabled={createRoomDisabled} onClick={this.handleLoginRoom} >Login Room</button>
                </fieldset>
              </form>
            </div>
          </div>

        </div>
        {this.state.showError && (
          <Error
            closeError={this.closeError}
            errorMsg={this.state.errorMsg}
          />
        )}
      </React.Fragment>
    )
  }
}

export default withRouter(Login);
