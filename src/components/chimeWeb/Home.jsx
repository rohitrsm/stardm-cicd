import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
// Components
import VideoPlayer from '../videoPlayer/VideoPlayer';
import ChatChime from '../chat/ChatChime';
import Layout from './Layout';
import Testimonial from './Testimonial';
// Styles
import './ChimeWeb.css';
import './style.css';

class Home extends Component {

  state = {
    playbackURL: localStorage.getItem('playbackURL'),
    meetingStatus: null, // Loading, Success or Failed
    showSettings: false,
    showError: false,
    errorMsg: '',
  }

  constructor() {
    super();
    this.baseHref = localStorage.getItem("starusername");
    this.ssName = '';
    this.joinInfo = '';
    this.role = '';
    this.audioElementRef = React.createRef();
    this.myVideoElement = React.createRef();
    sessionStorage.setItem("activeChatRoomIndex", -1);
    this.title = localStorage.getItem('title');
    this.isGlobalCreatedRoom = sessionStorage.getItem('isGlobalCreatedRoom');
    this.playbackURL = localStorage.getItem('playbackURL');
    this.username = localStorage.getItem("username");
  }
  componentDidMount() {
    
    //set cookie on other domain for test
    // this.setCookieUserInfo();

    //get the user info from subdomain cookie
    // const userInfo = this.getCookieUserInfo();

    //set userinfo localstorage
    // localStorage.setItem("username", "AAA");
    // localStorage.setItem("avatar", "https://brainshares.s3-us-west-2.amazonaws.com/1599930106_935123_avatar4.jpg");
    // localStorage.setItem("ivs_id", "room_test");
    // localStorage.setItem("level", 0);
    
    // initial set for chatchime
    if (this.isGlobalCreatedRoom) {
      this.role='host';
      const room = this.title;
      if (room) {
        this.ssName = `chime[${room}]`;
        if (sessionStorage.getItem(this.ssName)) {
          const ssData = JSON.parse(sessionStorage.getItem(this.ssName));
          this.title = ssData.title;
          this.joinInfo = ssData.joinInfo;
        }
      }
    } else {
      this.role='attendee';
    }
  }

  setCookieUserInfo() {
    document.cookie = "username=John Doe";
    document.cookie = "avatar=https://brainshares.s3-us-west-2.amazonaws.com/1599930106_935123_avatar4.jpg";
    document.cookie = "ivs_id=room_test";
    document.cookie = "level=0";
  }

  getCookieUserInfo() {
    let username, avatar, ivs_id, level;
    const cookieInfo = document.cookie;
    if (cookieInfo) {
      const cookieValues = document.cookie.split('; ');
      if (cookieValues.find(row => row.startsWith('username='))) {
        username = cookieValues.find(row => row.startsWith('username=')).split('=')[1];
      }
      if (cookieValues.find(row => row.startsWith('avatar='))) {
        avatar = cookieValues.find(row => row.startsWith('avatar=')).split('=')[1];
      }
      if (cookieValues.find(row => row.startsWith('ivs_id='))) {
        ivs_id = cookieValues.find(row => row.startsWith('ivs_id=')).split('=')[1];
      }
      if (cookieValues.find(row => row.startsWith('level='))) {
        level = cookieValues.find(row => row.startsWith('level=')).split('=')[1];
      }
    }
    return {username, avatar, ivs_id, level};
  }
  /*
   * Settings
   */
  setMetadataId = (metadataId) => {
    this.setState({ metadataId });
  }

  render() {
    return (
      <React.Fragment>
        <Layout>
          <div className="app-grid" onClick={this.handleClick}>
            <div className="main-stage">
              <VideoPlayer
                setMetadataId={this.setMetadataId}
                videoStream={this.playbackURL}
              />
              <Testimonial />
            </div>
            <ChatChime
              chime={this.props.chime}
              title={this.title}
              username={this.username}
              joinInfo={this.joinInfo}
              baseHref={this.baseHref}
              ssName={this.ssName}
              role={this.role}
              history={this.props.history}
              myVideoElement={this.myVideoElement}
              end={'a'}
            />
          </div>
        </Layout>
      </React.Fragment>
    )
  }
}

Home.propTypes = {
  chime: PropTypes.object
};

export default withRouter(Home);
