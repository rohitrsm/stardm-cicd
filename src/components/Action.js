export const STANDARDROOM = "STANDARDROOM";
export const VIPROOM = "VIPROOM";
export const CREATROOM = "CREATROOM";

export function standardRoom() {
  return { type: STANDARDROOM };
}

export const vipRoom = () => {
  return { type: VIPROOM };
}

export const creatRoom = () => {
  return { type: CREATROOM };
}