import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  DefaultPromisedWebSocketFactory,
  DefaultDOMWebSocketFactory,
  FullJitterBackoff,
  ReconnectingPromisedWebSocket,
  // ConsoleLogger
} from 'amazon-chime-sdk-js';
import * as config from '../../config';
// Styles
import './ChatChime.css';
//Stream Chat Tab
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import LockOpenOutlinedIcon from '@material-ui/icons/LockOpenOutlined';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { withRouter } from 'react-router-dom';
//redux for standard and vip room
import { connect } from 'react-redux';
import { standardRoom, vipRoom, creatRoom } from '../Action';
//vip room card
import VipJoinRoomCard from './VipJoinRoomCard';
import VipCreateMasterCard from './VipCreateMasterCard';
import VipCustomHeaderCard from './VipCustomHeaderCard';
import VipMasterHeaderCard from './VipMasterHeaderCard';
import UserList from './UserList';
import Controls from '../chimeWeb/Controls';
//Stream Chat
import { Chat, Channel, ChannelHeader, Window } from 'stream-chat-react';
import { MessageList, MessageInput, MessageLivestream } from 'stream-chat-react';
import { MessageInputSmall, Thread } from 'stream-chat-react';
import { StreamChat } from 'stream-chat';
import 'stream-chat-react/dist/css/index.css';
const jwt = require('jsonwebtoken');

class ChatChime extends Component {
  constructor() {
    super();
    this.WEB_SOCKET_TIMEOUT_MS = 10000;

    this.state = {
      message: '',
      messages: [],
      connection: null,
      showPopup: false,
      standardChatClient: null,
      standardChannel: null,
      vipChatClient: null,
      vipChannel: null,
      vipRoomId: null,
      masterChatClient: null,
      masterChannel: null,
      tabIndex: 0,
      activeChatRoomIndex: JSON.parse(sessionStorage.getItem("activeChatRoomIndex")),
      isGlobalCreatedRoom: JSON.parse(sessionStorage.getItem("isGlobalCreatedRoom")),
      createdRoomList: [],
      creatVipMemberInfo: [],
      users: []
    }

    this.chatRef = React.createRef();
    this.messagesEndRef = React.createRef();
    this.baseHref = localStorage.getItem("starusername");
    this.avatar = localStorage.getItem("avatar");
    this.title = localStorage.getItem("title");
    this.level = parseInt(localStorage.getItem("level"));
    this.userName = localStorage.getItem("username");
  }

  componentDidMount() {
    this.globalVariableSetting();
    this.initialStreamChat();
    this.getAllCreatedRoomList();
  }

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps.joinInfo) !== JSON.stringify(this.props.joinInfo)) {
      if (this.props.joinInfo) {
        this.handleCreateRoom();
        this.handleVipRoom();
      }
    }
  }
  
  componentWillReceiveProps(nextProps){    
    this.setState({
      activeChatRoomIndex: nextProps.activeChatRoomIndex,
    })
    if (this.state.activeChatRoomIndex != nextProps.activeChatRoomIndex) {
      this.getAllCreatedRoomList();
    }
    
    if (JSON.stringify(nextProps.joinInfo) !== JSON.stringify(this.props.joinInfo)) {
      if (this.props.joinInfo) {
        // this.handleCreateRoom();
        this.handleVipRoom();
      }
    }    
  }

  globalVariableSetting() {    
    if (this.state.isGlobalCreatedRoom) {
      if (!this.title || !sessionStorage.getItem(`chime[${this.title}]`)) {
        this.props.history.push(`/${this.baseHref}/`);
        sessionStorage.clear();
      }
      const currentUserInfo = JSON.parse(sessionStorage.getItem(`chime[${this.title}]`));
      if (currentUserInfo) {
        const creatVipMemberInfo = {
          avatar: currentUserInfo.avatarURL,
          title: currentUserInfo.subjectName,
          subheader: currentUserInfo.subjectDescription,
        };
        this.setState({
          creatVipMemberInfo,
        })
      }
    }

    if (this.props.tabGlobalIndex !== 0) {
      this.setState({
        tabIndex: 1,
      })
    }
  }

  async initialStreamChat() {
    if (!this.userName) {      
      this.props.history.push(`/`);
    } else {
      const room_id = localStorage.getItem("ivs_id"); //payblckurl ID;
      const standardChat = await this.streamChatEngine(this.avatar, this.userName, room_id);
      this.setState({
        standardChatClient: standardChat.chatClient,
        standardChannel: standardChat.channel,
      })
    }
  }

  async streamChatEngine(avatar, userName, room_id, isAdmin) {
    let user_name = userName.replaceAll(" ", "-");
    const userToken = jwt.sign({ user_id: user_name }, config.ACCESS_TOKEN_SECRET);
    const chatClient = new StreamChat(config.ACCESS_API_KEY, { timeout: 10000 });
    if (avatar === null || avatar === '') {
      avatar = `https://getstream.io/random_png/?id=${user_name}&name=${user_name.replaceAll("-", "+")}`;
    }
    try {
      await chatClient.setUser(
        {
          id: user_name,
          name: userName,
          image: avatar,
        },
        userToken
      );
      const channel = await chatClient.channel("messaging", room_id + "", {
        image: avatar,
        name: "Live Chat",
      });
      await channel.watch();

      if (isAdmin) await channel.enableSlowMode(20);
      return { chatClient, channel };
    } catch (error) {
      console.error(error);
      return {};
    }
  }

  async getAllCreatedRoomList() {
    const playbackURL = localStorage.getItem('playbackURL');
    const param = {
      playbackURL: playbackURL
    };
    const response = await fetch(
      `${config.CHIME_ROOM_API}/meetings`,
      {
        method: 'POST',
        body: JSON.stringify(param)
      }
    );
    const json = await response.json();
    if (json.error) {
      throw new Error(
        json.error
      );
    }
    this.setState({
      createdRoomList: json
    });
  }

  handleTabChange = (tabIndex) => {
    const { isGlobalCreatedRoom } = this.state;
    this.setState({
      tabIndex: tabIndex
    });
    switch (tabIndex) {
      case 0:
        this.props.standardRoom();
        break;
      case 1:
        if (!isGlobalCreatedRoom) {
          this.props.vipRoom();
          this.getAllCreatedRoomList();
        } else {
          this.props.creatRoom();
        }
        break;
    }
    this.props.handleTabChange();
  }

  async handleJoin(id) {
    const { vipRoomId } = this.state;
    this.setState({
      activeChatRoomIndex: id,
    });    
    const { createdRoomList } = this.state;
    if (id >= 0) {
      const meetingInfo = await JSON.parse(createdRoomList[id].Data.S);
      const room_id = meetingInfo.Meeting.MeetingId;
      this.setState({
        vipRoomId: room_id
      });
      if (room_id != vipRoomId) {
        const vipChat = await this.streamChatEngine(this.avatar, this.userName, room_id);
        this.setState({
          vipChatClient: vipChat.chatClient,
          vipChannel: vipChat.channel,
        });
      }
      await this.props.start();
    }    
  }

  kickUser = (user) => {
    this.props.chime.kickUser(user);
  }

  handleCreateMasterRoom = () => {
    const title = localStorage.getItem('title');
    const currentUserInfo = JSON.parse(sessionStorage.getItem(`chime[${title}]`));
    const creatVipMemberInfo = {
      avatar: currentUserInfo.avatarURL,
      title: currentUserInfo.subjectName,
      subheader: currentUserInfo.subjectDescription,
    }

    this.setState({
      isGlobalCreatedRoom: true,
      creatVipMemberInfo,
    });
    sessionStorage.setItem("isGlobalCreatedRoom", true);
    this.props.start();
  }

  handleVipRoom = async() => {
    const id = this.state.activeChatRoomIndex;
    if (id >= 0 && this.state.createdRoomList.length > 0 && !this.state.isGlobalCreatedRoom) {
      const meetingInfo = await JSON.parse(this.state.createdRoomList[id].Data.S);
      const room_id = meetingInfo.Meeting.MeetingId;
      this.setState({
        vipRoomId: room_id
      });
      if (room_id != this.state.vipRoomId || !this.state.vipChannel || !this.state.vipChatClient) {
        const vipChat = await this.streamChatEngine(this.avatar, this.userName, room_id);
        this.setState({
          vipChatClient: vipChat.chatClient,
          vipChannel: vipChat.channel,
        });
        this.props.start();
      }
    }
  }

  handleCreateRoom = async () => {
    const { isGlobalCreatedRoom } = this.state;
    const room = localStorage.getItem("title");
    if (isGlobalCreatedRoom) {
      this.ssName = `chime[${room}]`;
      if (!room || !sessionStorage.getItem(this.ssName)) {
        this.props.history.push(`/${this.baseHref}/`);
        sessionStorage.clear();
      }
      const room_id = await this.props.joinInfo.Meeting.MeetingId;
      const masterChat = await this.streamChatEngine(this.avatar, this.userName, room_id, true);
      this.setState({
        masterChatClient: masterChat.chatClient,
        masterChannel: masterChat.channel,
      })
    }
  }

  exitVipChatRoom = () => {
    this.setState({
      activeChatRoomIndex: -1,
    })
    sessionStorage.setItem("activeChatRoomIndex", -1);
    this.getAllCreatedRoomList();
    this.props.handleExitRoom();
  }

  endMasterChatRoom = () => {
    this.setState({
      isGlobalCreatedRoom: false,
    })
    this.props.endMasterChatRoom();
  }

  render() {
    const { standardChatClient, standardChannel,
      vipChatClient, vipChannel,
      masterChatClient, masterChannel,
      tabIndex, createdRoomList, 
      isGlobalCreatedRoom, activeChatRoomIndex, creatVipMemberInfo } = this.state;
    const { chime, baseHref, ssName, title, role, history, myVideoElement, openSettings, meetingStatus, isConference } = this.props;
    // const { showPopup } = this.state;
    // const popup = showPopup ? 'show' : '';    
    return (
      <div className="chat full-height pos-relative">
        <Tabs selectedIndex={tabIndex} onSelect={tabIndex => this.handleTabChange(tabIndex)}>
          <TabList>
            <Tab>Standard Room</Tab>
            {
              (this.level === 1 || this.level === 0) && <Tab>Vip Room {tabIndex === 0 ? <LockOutlinedIcon /> : <LockOpenOutlinedIcon />}</Tab>
            }
            {
              this.level === 2 && <Tab>Create Room</Tab>
            }
            {
              this.level == 2 && isGlobalCreatedRoom && <Tab>User List</Tab>
            }
          </TabList>
          <TabPanel>
            <Chat client={standardChatClient} theme='livestream dark'>
              <Channel channel={standardChannel} Message={MessageLivestream}>
                <Window hideOnThread>
                  <ChannelHeader live image={this.avatar} title={'Live STARDM'} />
                  <MessageList />
                  <MessageInput Input={MessageInputSmall} focus />
                </Window>
                <Thread fullWidth />
              </Channel>
            </Chat>
          </TabPanel>
          {
            (this.level === 1 || this.level === 0) && 
            <TabPanel>
              { (activeChatRoomIndex < 0 || activeChatRoomIndex === null) &&
                <div className="vip-room-scroll">
                  {
                    createdRoomList.map((data, index) => {
                      return (
                        <VipJoinRoomCard createdRoomList={data} handleJoin={(id) => this.handleJoin(id)} id={index} level={this.level} />
                      );
                    })
                  }
                </div>
              }
              {activeChatRoomIndex >= 0 && meetingStatus === 'Success' && createdRoomList[activeChatRoomIndex] &&
                  <Chat client={vipChatClient} theme='livestream dark'>
                    <Channel channel={vipChannel} Message={MessageLivestream}>
                      <Window hideOnThread>
                        <div className="vip-room-active">
                          <VipCustomHeaderCard data={createdRoomList[activeChatRoomIndex]} />
                        </div>
                        <MessageList />
                        <div className="exit-active-room">
                            <Controls
                              chime={chime}
                              baseHref={baseHref}
                              ssName={ssName}
                              title={title}
                              openSettings={openSettings}
                              role={role}
                              history={history}
                              myVideoElement={myVideoElement}
                              exitVipChatRoom={() => this.exitVipChatRoom()}
                              endMasterChatRoom = {() => this.endMasterChatRoom()}
                              activeChatRoomIndex={activeChatRoomIndex}
                              isGlobalCreatedRoom={isGlobalCreatedRoom}
                              isConference={isConference}
                            />
                        </div>
                        <MessageInput Input={MessageInputSmall} focus />
                      </Window>
                      <Thread fullWidth />
                    </Channel>
                  </Chat>
              }
            </TabPanel>}
          {
            this.level === 2 && 
            <TabPanel>
              {!isGlobalCreatedRoom &&
                <VipCreateMasterCard handleCreateMasterRoom={() => this.handleCreateMasterRoom()} />
              }
              {isGlobalCreatedRoom && meetingStatus === 'Success' &&
                <Chat client={masterChatClient} theme='livestream dark'>
                  <Channel channel={masterChannel} Message={MessageLivestream}>
                    <Window hideOnThread>
                      <div className="vip-room-active">
                        <VipMasterHeaderCard data={creatVipMemberInfo} />
                      </div>
                      <MessageList />
                      <div className="exit-active-room">
                          <Controls
                            chime={chime}
                            baseHref={baseHref}
                            ssName={ssName}
                            title={title}
                            openSettings={openSettings}
                            role={role}
                            history={history}
                            myVideoElement={myVideoElement}
                            exitVipChatRoom={() => this.exitVipChatRoom()}
                            endMasterChatRoom = {() => this.endMasterChatRoom()}
                            isGlobalCreatedRoom={isGlobalCreatedRoom}
                            activeChatRoomIndex={activeChatRoomIndex}
                            isConference={isConference}
                          />
                      </div>
                      <MessageInput Input={MessageInputSmall} focus />
                    </Window>
                    <Thread fullWidth />
                  </Channel>
                </Chat>
              }
            </TabPanel>
          }
          {
            this.level == 2 && isGlobalCreatedRoom && meetingStatus === 'Success' && <TabPanel>
              <UserList client={masterChatClient} kickUser={(user) => this.kickUser(user)}>
              </UserList>
            </TabPanel>
          }
        </Tabs>
      </div>
    )
  }
}

ChatChime.propTypes = {
  chime: PropTypes.object,
  title: PropTypes.string,
  username: PropTypes.string,
  joinInfo: PropTypes.object
};

function mapState(state) {
  return {
    tabGlobalIndex: state.tabGlobalIndex,
  };
}

const mapDispatch = {
  standardRoom, vipRoom, creatRoom
};

export default withRouter(connect(mapState, mapDispatch)(ChatChime));
