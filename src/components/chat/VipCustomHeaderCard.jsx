import React, { Component } from 'react';
import { Card, CardHeader } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import { withStyles } from '@material-ui/styles';
import * as config from '../../config';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { creatRoom, vipRoom } from '../Action';

const useStyles = theme => ({
	root: {
		maxWidth: 345,
		borderRadius: '10px',
		margin: '15px',
		background: '#F0EFF4',
	},
	cardheader: {
		'& .MuiCardHeader-title': {
			fontWeight: '600',
			color: '#1b1b1b',
			fontSize: '18px',
			fontVariant: 'all-petite-caps'
		},
		'& .MuiCardHeader-subheader': {
			fontSize: '12px',
			color: '#6f6c6c',

		},
	}
});

class VipCustomHeaderCard extends Component {
	constructor() {
		super();
		this.inputRef = React.createRef();
	}

	render() {
		const { data } = this.props;
		const { classes } = this.props;
		return (
			<Card className={classes.root}>
				<CardHeader
					className={classes.cardheader}
					avatar={
						<Avatar aria-label="recipe" alt="vip avatar" src={data.AvatarURL.S} />
					}
					title={data.SubjectName.S}
					subheader={data.SubjectDescription.S}
				/>
			</Card>
		)
	}
}

function mapState(state) {
	return {
		isGlobalCreatedRoom: state.isGlobalCreatedRoom
	};
}

const mapDispatch = {
	creatRoom, vipRoom,
};

export default connect(
	mapState,
	mapDispatch
)(withRouter(withStyles(useStyles)(VipCustomHeaderCard)));