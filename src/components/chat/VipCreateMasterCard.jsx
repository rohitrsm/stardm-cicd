import React, { Component } from 'react';
import { Card, CardContent } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/styles';
import * as config from '../../config';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { creatRoom } from '../Action';

const useStyles = theme => ({
  root: {
    borderRadius: '10px',
    margin: '15px',
  },
  cardheader: {
    '& .MuiCardHeader-title': {
      fontWeight: '600',
      color: '#1b1b1b',
      fontSize: '18px',
      fontVariant: 'all-petite-caps'
    },
    '& .MuiCardHeader-subheader': {
      fontSize: '12px',
      color: '#6f6c6c',

    },
  },
  cardbutton: {
    fontSize: '12px',
    textTransform: 'none',
    color: 'white',
    fontWeight: 700,
    width: '100%',
    background: 'linear-gradient(45deg,#E56C01, #B70251)',
    display: 'flex',
    padding: '0px',
    borderRadius: '8px',
    height: '36px',
  },
  cardContent: {
    '&:last-child': {
      padding: '15px 15px 15px 15px'
    }
  },
  checkboxContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  checkbox: {
    appearance: 'checkbox',
    width: 'auto',
    margin: 'unset',
    marginRight: '10px'
  }
});

class VipCreateMasterCard extends Component {
  
  constructor() {
    super();

    this.state = {
      role: 'host',
      username: '',
      title: '',
      subjectName: "",
      subjectDescription: "",
      playbackURL: localStorage.getItem('playbackURL'),
      errorMsg: '',
      showError: false,
      isConference: false
    }

    this.baseHref = localStorage.getItem("starusername");
    this.inputRef = React.createRef();
  }

  componentDidMount() {
    this.setState({
      username: localStorage.getItem("username"),
    });
  }

  componentWillMount() {
      this.id = `toggle_${Math.random().toString().replace(/0\./, '')}`;
  }

  handleNameChange = e => {
    this.setState({ username: e.target.value })
  }

  handleRoomChange = e => {
    this.setState({ title: e.target.value })
  }
  handleSubjectNameChange = e => {
    this.setState({ subjectName: e.target.value })
  }

  handleSubjectDescriptionChange = e => {
    this.setState({ subjectDescription: e.target.value })
  }

  handleConferenceChange = () => {
    this.setState({
      isConference: !this.state.isConference
    })
  }

  handleCreateMasterRoom = async() => {
    this.props.creatRoom();
    const { title, username, playbackURL, subjectName, subjectDescription, isConference } = this.state;
    const data = {
      username,
      title,
      subjectName,
      subjectDescription,
      playbackURL,
      role: this.state.role,
      avatarURL: localStorage.getItem("avatar"),
      isConference: isConference
    }
    localStorage.setItem("title", title);
    sessionStorage.setItem(`chime[${title}]`, JSON.stringify(data));
    await this.props.handleCreateMasterRoom();
  }

  render() {
    const { classes } = this.props;
    const { title, username, subjectName, subjectDescription, isConference } = this.state;
    const createRoomDisabled = !username || !title;
    return (
      <Card className={classes.root}>
        <form action="">
          <fieldset className="mg-b-2 creat-room-field">
            {/* <input type="text" placeholder="Your name" value={username} ref={this.inputRef} onChange={this.handleNameChange} /> */}
            <input type="text" placeholder="Subject name" value={subjectName} onChange={this.handleSubjectNameChange} />
            <input type="text" placeholder="Subject description" value={subjectDescription} onChange={this.handleSubjectDescriptionChange} />
            <input type="text" placeholder="Room name" value={title} onChange={this.handleRoomChange} />
            <div  className={classes.checkboxContainer}>
              <input className={classes.checkbox} onChange={this.handleConferenceChange} id={this.id} type="checkbox" checked={isConference} />
                <label htmlFor={this.id}>Conferencing Enabled</label>
            </div>
          </fieldset>
        </form>
        <CardContent className={classes.cardContent}>
          <Button size="large" color="primary" disabled={createRoomDisabled} className={classes.cardbutton} onClick={this.handleCreateMasterRoom}>
            Create Room
          </Button>
        </CardContent>

      </Card>
    )
  }
}

function mapState(state) {
  return {
    isGlobalCreatedRoom: state.isGlobalCreatedRoom
  };
}

const mapDispatch = {
  creatRoom
};

export default connect(
  mapState,
  mapDispatch
)(withRouter(withStyles(useStyles)(VipCreateMasterCard)));