import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import { Typography } from '@material-ui/core';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon  from '@material-ui/icons/Delete';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    color: 'black',
    padding: '6px 24px !important'
  },
  rootButton: {

  }
}));

export default function UserList(props) {
  const classes = useStyles();
  const [users, setUsers] = useState([])

  const fetchUsers = async () => {
    const response = props.client && await props.client.queryUsers({
    }, {
      last_active: -1
    }, {
      presence: true
    });

    const channels = props.client && await props.client.queryChannels({}, {}, {
      watch: true, // this is the default
      state: true,
    }); 

    response && setUsers(response.users);
  }

  const kickUser = async (user) => {
    props.kickUser(user);
    let index = users.findIndex(u => JSON.stringify(u) === JSON.stringify(user));
    if (index > 0) {
      let usersTemp = users.slice(0);
      usersTemp.splice(index, 1);
      setUsers(usersTemp);
    }
  }

  useEffect(() => {
    fetchUsers();
  }, [])

  if (users.length == 0) 
    return <></>;

  return (
    <List className={classes.root}>
      {users.map((user) => {
        const labelId = `checkbox-list-label-${user.id}`;

        return (
          <ListItem key={user} dense>
            <ListItemText
              id={labelId}
              disableTypography
              primary={
                <Typography variant="h4" style={{ color: "black" }}>
                  {user.role === "admin" ? (user.name ? user.name : user.id) + " (Admin)" : (user.name ? user.name : user.id) }
                </Typography>
              }
            />
            <ListItemSecondaryAction>
              {user.role !== "admin" && (
                <IconButton
                  classes={{
                    root: classes.rootButton,
                  }}
                  edge="end"
                  aria-label="delete"
                  onClick={() => kickUser(user)}
                >
                  <DeleteIcon style={{ fontSize: "2.125rem" }} />
                </IconButton>
              )}
            </ListItemSecondaryAction>
          </ListItem>
        );
      })}
    </List>
  );
}