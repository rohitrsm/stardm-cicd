import React from 'react';
import { Card, CardHeader, CardContent } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import { Component } from 'react';
import { withStyles } from '@material-ui/styles';
import { withRouter } from 'react-router-dom';

const useStyles = theme => ({
  root: {
    maxWidth: 345,
    borderRadius: '10px',
    margin: '15px',
    background: '#F0EFF4',
  },
  cardheader: {
    '& .MuiCardHeader-title': {
      fontWeight: '600',
      color: '#1b1b1b',
      fontSize: '18px',
      fontVariant: 'all-petite-caps'
    },
    '& .MuiCardHeader-subheader': {
      fontSize: '12px',
      color: '#6f6c6c',

    },
  },
  cardbutton: {
    fontSize: '12px',
    textTransform: 'none',
    color: 'white',
    fontWeight: 700,
    width: '100%',
    background: 'linear-gradient(45deg,#E56C01, #B70251)',
    display: 'flex',
    padding: '0px',
    borderRadius: '8px',
    height: '36px',
  },
  cardContent: {
    '&:last-child': {
      padding: '0 15px 15px 15px'
    }
  }
});

class VipJoinRoomCard extends Component {

  
  constructor() {
    super();

    this.state = {
      role: 'attendee',
      username: localStorage.getItem("username"),
      title: '',
      playbackURL: localStorage.getItem('playbackURL'),
      errorMsg: '',
      showError: false
    }
    
    this.inputRef = React.createRef();
    this.baseHref = localStorage.getItem("starusername");
  }

  handleJoinRoom = async () => {
    const { handleJoin } = this.props;
    const { createdRoomList, id } = this.props;
    const title = createdRoomList.Title.S;
    const { username, playbackURL, role } = this.state;
    const data = {
      username,
      title,
      playbackURL,
      role,
      avatarURL: localStorage.getItem("avatar"),
      isConference: createdRoomList.isConference.BOOL,
    }
    localStorage.setItem('title', title);
    sessionStorage.setItem("activeChatRoomIndex", id);
    sessionStorage.setItem(`chime[${title}]`, JSON.stringify(data));
    await handleJoin(id);
  }

  render() {
    const { classes } = this.props;
    const { createdRoomList, level } = this.props;
    return (
      <Card className={classes.root}>
        <CardHeader
          className={classes.cardheader}
          avatar={
            <Avatar aria-label="recipe" alt="vip avatar" src={createdRoomList.AvatarURL.S} />
          }
          title={createdRoomList.SubjectName.S}
          subheader={createdRoomList.SubjectDescription.S}
        />
        <CardContent className={classes.cardContent}>
          {
            level === 0 ?
            <Button size="large" color="primary" className={classes.cardbutton} disabled>
              Join
            </Button>
            :
            <Button size="large" color="primary" className={classes.cardbutton} onClick={this.handleJoinRoom} >
              Join
            </Button>
          }
        </CardContent>
      </Card>
    );
  }
}

export default withRouter(withStyles(useStyles)(VipJoinRoomCard));