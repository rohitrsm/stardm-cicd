import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as config from '../../config';
import { withStyles } from '@material-ui/styles';
//import videoSrc from '../../assets/video.mp4';
import FullscreenIcon from '@material-ui/icons/Fullscreen';
import FullscreenExitIcon from '@material-ui/icons/FullscreenExit';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import VolumeUpIcon from '@material-ui/icons/VolumeUp';
import VolumeOffIcon from '@material-ui/icons/VolumeOff';
import SettingsIcon from '@material-ui/icons/Settings';
import {Button, IconButton} from '@material-ui/core';
import CastButton from './CastButton';
import CastProvider from 'react-chromecast';
import './VideoPlayer.css';

// Styles
const useStyles = theme => ({
  icon: {
    color: 'white',
    width: '24px',
    height: '24px'
  },
  icon_container: {
    visibility: 'hidden',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1,
    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0.3)',
      '& $icon_container': {
        visibility: 'visible'
      }
    }
  },
});

class VideoPlayer extends Component {

  constructor() {
    super ();
    this.videoElement = React.createRef();
    this.state = {
      player: null,
      maxMetaData: 10,
      metaData: [],
      isPlaying: true, 
      isMuted: false,
      isFullScreen: false,
    }
  }

  componentDidMount() {

    const mediaPlayerScript = document.createElement("script");
    mediaPlayerScript.src = "https://player.live-video.net/1.2.0/amazon-ivs-player.min.js";
    mediaPlayerScript.async = true;
    mediaPlayerScript.onload = () => this.mediaPlayerScriptLoaded();
    document.body.appendChild(mediaPlayerScript);
  }


  mediaPlayerScriptLoaded = () => {
    // This shows how to include the Amazon IVS Player with a script tag from our CDN
    // If self hosting, you may not be able to use the create() method since it requires
    // that file names do not change and are all hosted from the same directory.

    const MediaPlayerPackage = window.IVSPlayer;

 //const playerOverlay = document.getElementById("overlay"); 
    const btnSettings = document.getElementById("settings");
    const settingsMenu = document.getElementById("settings-menu");
    // Create Quality Options
    let createQualityOptions = function (obj, i) {
      let q = document.createElement("a");
      let qText = document.createTextNode(obj.name);
      settingsMenu.appendChild(q);
      q.classList.add("settings-menu-item");
      q.appendChild(qText);

      q.addEventListener("click", (event) => {
        player.setQuality(obj);
        closeSettingsMenu();
        while (settingsMenu.firstChild)
          settingsMenu.removeChild(settingsMenu.firstChild);
        return false;
      });
    };

    // Close Settings menu
    let closeSettingsMenu = function () {
      btnSettings.classList.remove("btn--settings-on");
      btnSettings.classList.add("btn--settings-off");
      settingsMenu.classList.remove("open");
    };

    // Settings
    btnSettings.addEventListener(
      "click",
      function (e) {
        let qualities = player.getQualities();
        let currentQuality = player.getQuality();

        // Empty Settings menu
        while (settingsMenu.firstChild)
          settingsMenu.removeChild(settingsMenu.firstChild);

        if (btnSettings.classList.contains("btn--settings-off")) {
          for (var i = 0; i < qualities.length; i++) {
            createQualityOptions(qualities[i], i);
          }
          btnSettings.classList.remove("btn--settings-off");
          btnSettings.classList.add("btn--settings-on");
          settingsMenu.classList.add("open");
        } else {
          closeSettingsMenu();
        }
      },
      false
    );

    

    // First, check if the browser supports the Amazon IVS player.
    if (!MediaPlayerPackage.isPlayerSupported) {
        console.warn("The current browser does not support the Amazon IVS player.");
        return;
    }
    const PlayerState = MediaPlayerPackage.PlayerState;
    const PlayerEventType = MediaPlayerPackage.PlayerEventType;

    // Initialize player
    const player = MediaPlayerPackage.create();
    
    player.attachHTMLVideoElement(document.getElementById("video-player"));

    // Attach event listeners
    player.addEventListener(PlayerState.PLAYING, function () {
      if (config.DEBUG) console.log("Player State - PLAYING");
    });
    player.addEventListener(PlayerState.ENDED, function () {
      if (config.DEBUG) console.log("Player State - ENDED");
    });
    player.addEventListener(PlayerState.READY, function () {
      if (config.DEBUG) console.log("Player State - READY");
    });
    player.addEventListener(PlayerEventType.ERROR, function (err) {
      if (config.DEBUG) console.warn("Player Event - ERROR:", err);
    });

    player.addEventListener(PlayerEventType.TEXT_METADATA_CUE, function (cue) {
      const metadataText = cue.text;
      const position = player.getPosition().toFixed(2);
      if (config.DEBUG) console.log(
        `Player Event - TEXT_METADATA_CUE: "${metadataText}". Observed ${position}s after playback started.`
      );
    });

    // Setup stream and play
    player.setAutoplay(true);
    player.load(this.props.videoStream);
  
    // Setvolume
    player.setVolume(0.3);
    this.setState({player: player});
    
  }
  
  togglePlayStatus = () => {
    const isPlaying = this.state.isPlaying;
    isPlaying ? this.state.player.pause() : this.state.player.play();
    this.setState({isPlaying: !this.state.isPlaying});
  }

  toggleMuteMode = () => {
    const isMuted = this.state.isMuted;
    this.state.player.setMuted(!isMuted);
    this.setState({isMuted: !this.state.isMuted});
  }

  toggleScreenMode = () => {
    this.setState({isFullScreen: !this.state.isFullScreen});
    var elem = document.getElementById("video-player-with-controls");
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.webkitRequestFullscreen) { /* Safari */
      elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE11 */
      elem.msRequestFullscreen();
    }
  }
  exitScreenMode = () => {
    this.setState({isFullScreen: !this.state.isFullScreen});
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.webkitExitFullscreen) { /* Safari */
      document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { /* IE11 */
      document.msExitFullscreen();
    }
  }




  render() {
    const { classes } = this.props;
    return (
        <CastProvider>
          {
            <div id="video-player-with-controls" className="player-wrapper pos-relative">
              <div className="aspect-spacer"></div>
              <div className="pos-absolute full-width full-height top-0">
                <div className={classes.overlay}>
                  <div className={classes.icon_container}>
                    <IconButton aria-label="Toggle Play Status" onClick={this.togglePlayStatus}>
                      {this.state.isPlaying ? <PauseIcon className={classes.icon}/> :
                          <PlayArrowIcon className={classes.icon}/>}
                    </IconButton>

                    <IconButton aria-label="Toggle Volume Status" onClick={this.toggleMuteMode}>
                      {this.state.isMuted ? <VolumeOffIcon className={classes.icon}/> :
                          <VolumeUpIcon className={classes.icon}/>}
                    </IconButton>
                    <IconButton aria-label="Toggle Screen Mode" >
                      {this.state.isFullScreen ? <FullscreenExitIcon onClick={this.exitScreenMode} className={classes.icon}/> :
                          <FullscreenIcon onClick={this.toggleScreenMode}  className={classes.icon}/>}
                    </IconButton>
                  <CastButton />
                    <IconButton id="settings" aria-label="Toggle quality Selector " onClick={this.toggleQuality}>
                      {this.state.currentQuality ? <SettingsIcon className={classes.icon}/> :
                          <SettingsIcon className={classes.icon}/>}
                    </IconButton>
                    <span id="settings-menu"></span>
                  </div>
                </div>
                <video id="video-player" className="el-player" ref={this.videoElement} playsInline loop></video>
              </div>
            </div>
          }
        </CastProvider>
    )
  }
}

VideoPlayer.propTypes = {
  setMetadataId: PropTypes.func,
  videoStream: PropTypes.string,
};

export default (withStyles(useStyles)(VideoPlayer));
