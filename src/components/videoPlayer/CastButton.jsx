import React, {Component, useCallback, useState} from 'react';
import {useCast} from 'react-chromecast';
import CastIcon from '@material-ui/icons/Cast';
import CastConnectedIcon from '@material-ui/icons/CastConnected';
import {IconButton} from "@material-ui/core";
import {withStyles} from "@material-ui/styles";

const useStyles = theme => ({
    icon: {
        color: 'white',
        width: '24px',
        height: '24px'
    },
    icon_container: {
        visibility: 'hidden',
    },
    overlay: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 1,
        '&:hover': {
            backgroundColor: 'rgba(0,0,0,0.3)',
            '& $icon_container': {
                visibility: 'visible'
            }
        }
    },
});

function CastButton(props) {
    const [isCast, setIsCast] = useState(false);
    const {classes} = props;
    const cast = useCast({
        initialize_media_player: "DEFAULT_MEDIA_RECEIVER_APP_ID",
        auto_initialize: true,
    })
    function addCast() {
        setIsCast(true)
        handleClick()
    }
    const handleClick = useCallback(async () => {
        if (cast.castReceiver) {
            await cast.handleConnection();

        }
    }, [cast.castReceiver, cast.handleConnection])
    return (

        <IconButton aria-label="Toggle Cast Status" onClick={addCast}>
            { {isCast} ? <CastIcon className={classes.icon}/> :
                <CastConnectedIcon className={classes.icon}/>}
            </IconButton>
    )
}
export default (withStyles(useStyles)(CastButton));