import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import * as config from '../config';
// import './App.css';

import ChimeSdkWrapper from './chime/ChimeSdkWrapper';

import Home from './chimeWeb/Home';
// import Welcome from './chimeWeb/Welcome';
import Join from './chimeWeb/Join';
import Meeting from './chimeWeb/Meeting';
import End from './chimeWeb/End';
import Login from './chimeWeb/Login';
//redux
import { createStore } from 'redux';
import { Provider } from 'react-redux';

function App() {
  const chime = new ChimeSdkWrapper();
  return (
    <div className="App full-width full-height">
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path={`/end`}>
              <End />
            </Route>
            {/* <Route path={`/:id/meeting`}>
              <Meeting
                chime={chime}
              />
            </Route> */}
            {/* <Route path={`/:id/join`}>
              <Join
                chime={chime}
              />
            </Route> */}
            <Route path={`/:id`}>
              <Meeting
                chime={chime}
              />
            </Route>
            <Route exact path="/">
              <Login/>
            </Route>
            {/* <Route exact path="/">
              <Redirect to={`/1`} />
            </Route> */}
            {/* <Route path={``}>
              <Home
                chime={chime}
              />
            </Route> */}
          </Switch>
        </Router>
      </Provider>
    </div>
  );
}

const initialState = {
  tabGlobalIndex: 0,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case "STANDARDROOM":
      return {
        tabGlobalIndex: 0,
      };
    case "VIPROOM":
      return {        
        tabGlobalIndex: 1,
      };
    case "CREATROOM":
      return {        
        tabGlobalIndex: 2,
      };
    default:
      return state;
  }
}
const store = createStore(reducer);

export default App;
