export const BASE_HREF = "";

// API endpoint for retrieving the attendees list, joining the room, and ending the room
export const CHIME_ROOM_API = "https://xcve95iuie.execute-api.eu-west-1.amazonaws.com/Prod";

// Chime-SDK allows up to 16 attendee videos
export const CHIME_ROOM_MAX_ATTENDEE = 50;

// Default video stream to play inside the video player
export const DEFAULT_VIDEO_STREAM = "https://f30daea56c61.eu-west-1.playback.live-video.net/api/video/v1/eu-west-1.062424101843.channel.d1plCKAS8R6Y.m3u8";

// Default Chat websocket link
export const CHAT_WEBSOCKET = "wss://7d73sqcp0g.execute-api.eu-west-1.amazonaws.com/Prod/";

// Chime-SDK logging level: INFO, WARN, ERROR, DEBUG
export const CHIME_LOG_LEVEL = 'WARN';
// Chime-Web UI debugging logging: true / false
export const DEBUG = false;
//Streaming Chat
export const ACCESS_API_KEY = "dweg8retz2fn";
export const ACCESS_TOKEN_SECRET = "kd3239w7cgh8f642fsjk3herk7n8d8y4edngsky3bsb2bub3uc79rfarx7fcbjej";

export const AES_KEY = "StardmForFans\0\0\0";
export const AES_IV = "1234567891011121";

export const HOME_PAGE = "https://stardm.me";
