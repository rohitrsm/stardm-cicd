import * as config from '../config'

export default (text) => {
    let CryptoJS = window.CryptoJS;
    let str = text;
    str = decodeURIComponent(str);
    str = str.split("-").join("+");
    str = str.split("_").join("/");
    str = str.split(",").join("=");
    let encrypted = CryptoJS.enc.Base64.parse(str);
    encrypted = encrypted.toString(CryptoJS.enc.Utf8);
    encrypted = CryptoJS.enc.Base64.parse(encrypted);
    let key = CryptoJS.enc.Utf8.parse(config.AES_KEY);
    let iv = CryptoJS.enc.Utf8.parse(config.AES_IV);
    let decrypted = CryptoJS.AES.decrypt(
        {
            ciphertext: encrypted
        },
        key,
        {
            iv: iv,
            mode: CryptoJS.mode.CTR,
            padding: CryptoJS.pad.NoPadding
        });

    return decrypted.toString(CryptoJS.enc.Utf8);
}